@extends('layout.app')

@section('login')
    <div class="card mt-5 w-50">
        <div class="card-header">Register</div>
        <div class="card-body">
            <form action="{{ route('register-post') }}" method="POST">
                @csrf
                <div class="row align-items-center">
                    <div class="col-md-2"><label for="name" class="form-label">Name:</label></div>
                    <div class="col-md-10"><input type="text" id="name" name="name"
                            class="form-control @error('name') is-invalid @enderror" placeholder="Enter Your Name">
                        @error('name')
                            <span class="lead text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                </div>
                <div class="row align-items-center mt-4">
                    <div class="col-md-2"><label for="email" class="form-label">Email:</label></div>
                    <div class="col-md-10"><input type="email" id="email" name="email"
                            class="form-control @error('email') is-invalid @enderror"
                            placeholder="Enter Your Email Address">
                        @error('email')
                            <span class="lead text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row align-items-center mt-4">
                    <div class="col-md-2"><label for="password" class="form-label">Password:</label></div>
                    <div class="col-md-10"><input type="password" id="password" name="password"
                            class="form-control @error('password') is-invalid @enderror" placeholder="Enter Password">
                        @error('password')
                            <span class="lead text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row align-items-center mt-4">
                    <div class="col-md-2"><label for="confirm" class="form-label">Confirm Password:</label></div>
                    <div class="col-md-10"><input type="password" id="confirm" name="password_confirmation"
                            class="form-control @error('password_confirmation') is-invalid @enderror"
                            placeholder="Re-enter Password">
                        @error('password_confirmation')
                            <span class="lead text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
        </div>
        <div class="card-footer text-end">
            <button type="submit" class="btn btn-primary">Register</button>
        </div>
        </form>

    </div>
@endsection
