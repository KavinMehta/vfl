@extends('layout.app')

@section('login')
    <div class="card mt-5 w-50">
        <div class="card-header">Login</div>
        <div class="card-body">
            <form action="{{ route('login-post') }}" method="POST">
                @csrf
                {{-- @dd(Session::all()) --}}
                @if (Session('error-login'))
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <span class="text-danger fw-bold">{{ Session('error-login') }}</span>
                        </div>
                    </div>
                @endif
                <div class="row align-items-center">
                    <div class="col-md-2"><label for="email" class="form-label">Email:</label></div>
                    <div class="col-md-10"><input type="email" id="email" name="email"
                            class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                            placeholder="Enter Registered Email ID">
                        @error('email')
                            <span class="text-danger lead">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="row align-items-center mt-4">
                    <div class="col-md-2"><label for="password" class="form-label">Password:</label></div>
                    <div class="col-md-10"><input type="password" id="password" name="password"
                            class="form-control @error('password') is-invalid @enderror" placeholder="Enter Password">
                        @error('password')
                            <span class="text-danger lead">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
        </div>
        <div class="card-footer text-end">
            <button type="submit" class="btn btn-primary">Login</button>
        </div>
        </form>

    </div>
@endsection
