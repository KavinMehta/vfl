@extends('layout.app')

@section('login')
    @if (Session('user'))
        {{-- @dd(Session('user')) --}}
        <p>Hello {{ Session('user')->name }}</p>
        <form action="{{ route('logout') }}" method="POST">
            @csrf
            <button type="submit" class="btn btn-primary">Logout</button>
        </form>
    @else
        <p>Please Login </p>
    @endif
@endsection
