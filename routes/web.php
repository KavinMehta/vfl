<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', [App\Http\Controllers\LoginController::class, 'loginScreen'])->name('login');
Route::get('/register', [App\Http\Controllers\LoginController::class, 'registerScreen'])->name('register');
Route::post('/register', [App\Http\Controllers\LoginController::class, 'register'])->name('register-post');
Route::get('/home', [App\Http\Controllers\LoginController::class, 'home'])->name('home');
Route::post('/logout', [App\Http\Controllers\LoginController::class, 'logout'])->name('logout');
Route::post('/login', [App\Http\Controllers\LoginController::class, 'login'])->name('login-post');
