<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function loginScreen()
    {
        return view('auth.login');
    }
    public function registerScreen()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:200',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6',
        ]);
        $user = User::register($request);
        Session::put('user', $user);
        return view('home');
    }
    public function home()
    {
        return view('home');
    }
    public function logout(Request $request)
    {
        Session::remove('user');
        return redirect('/home');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $user = User::login($request);
        if ($user) {
            $logged_in = User::where('email', $request->email)->first();
            Session::put('user', $logged_in);
            return view('home');
        } else {
            return redirect()->back()->with('error-login', 'Invalid Credentials');
        }
    }
}
